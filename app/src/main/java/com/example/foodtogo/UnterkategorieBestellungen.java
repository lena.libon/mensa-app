package com.example.foodtogo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.database.Bestellung;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.LinkedList;
import java.util.List;

public class UnterkategorieBestellungen extends AppCompatActivity {

    private TextView ueberschriftBestellung;
    public RecyclerView recyclerView;

    private RecyclerViewAdapterBestellung adapterBestellung;


    FirebaseDatabase database;
    DatabaseReference bestellung;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unterkategorie_bestellungen);

        //init
        ueberschriftBestellung = findViewById(R.id.txt_bestellungskategorie);
        recyclerView = findViewById(R.id.recycler_view_bestellungen);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Firebase
        database = FirebaseDatabase.getInstance();
        bestellung = database.getReference("Bestellung");

        // Setze Überschrift, Lade passende Bestellungen
        StatusBestellung bestellungskategorie = null;
        if (getIntent() != null)
            bestellungskategorie = (StatusBestellung) getIntent().getSerializableExtra("bestellungsstatus");
        if (bestellungskategorie != null ) {
            ueberschriftBestellung.setText(bestellungskategorie.toString().toUpperCase());

            // Mehrere WHERE gehen bei Firebase nicht --> add Value Listener
            final List<Bestellung> listBestellung = new LinkedList<>();
            final List<String> listIDs = new LinkedList<>();

            final Query query = bestellung.orderByChild("status").equalTo(bestellungskategorie.toString());
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Bestellung bestellungTemp = snapshot.getValue(Bestellung.class);

                        if (bestellungTemp.getHandynummer().equals(AktiverNutzer.aktivernutzer.getHandynummer())) {
                            listBestellung.add(bestellungTemp);
                            listIDs.add(snapshot.getKey());
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Intent intent = new Intent(UnterkategorieBestellungen.this, FehlerDatenbankverbindung.class);
                    startActivity(intent);
                    finish();
                }
            });


            adapterBestellung = new RecyclerViewAdapterBestellung(this, listBestellung, listIDs);
            recyclerView.setAdapter(adapterBestellung);
        }
    }
}
