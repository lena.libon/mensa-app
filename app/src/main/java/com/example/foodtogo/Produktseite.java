package com.example.foodtogo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.foodtogo.database.Produkt;
import com.example.foodtogo.database_bestellung.DatabaseBestellung;
import com.example.foodtogo.database_bestellung.Teilbestellung;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class Produktseite extends AppCompatActivity {

    private FirebaseDatabase database;
    private DatabaseReference produkt;

    private Produkt produktAusgewaehlt;
    private String essenskategorieId = "";

    private ImageView imgProdukt;
    private TextView preisProdukt, zusatzstoffeProdukt;
    private ElegantNumberButton anzahlProdukt;
    private FloatingActionButton btnEinkaufswagen;
    private CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produktseite);

        // Zu Firebase verbinden
        database = FirebaseDatabase.getInstance();
        produkt = database.getReference("Produkt");

        imgProdukt = findViewById(R.id.img_food_produktseite);
        preisProdukt = findViewById(R.id.essenspreis_produktseite);
        zusatzstoffeProdukt = findViewById(R.id.angabe_zusatzstoffe_produktseite);
        anzahlProdukt = findViewById(R.id.anzahl_produkte_produktseite);
        btnEinkaufswagen = findViewById(R.id.btn_warenkorb_produktseite);

        btnEinkaufswagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                new DatabaseBestellung(getBaseContext()).addTeilbestellung(new Teilbestellung(
                        essenskategorieId,
                        produktAusgewaehlt.getName(),
                        anzahlProdukt.getNumber(),
                        produktAusgewaehlt.getPreis()
                ));

                Toast.makeText(Produktseite.this, "Produkt wurde erfolgreich zum Warenkorb hinzugefügt", Toast.LENGTH_SHORT).show();
            }
        });

        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar_layout_produktseite);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ToolbarGeoeffnet);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.ToolbarGeschlossen);



        if (getIntent() != null) {
            essenskategorieId = getIntent().getStringExtra("essenskategorieID");
        }if (!essenskategorieId.isEmpty()) {
            produkt.child(essenskategorieId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    produktAusgewaehlt = dataSnapshot.getValue(Produkt.class);

                    collapsingToolbarLayout.setTitle(produktAusgewaehlt.getName());
                    Picasso.get().load(produktAusgewaehlt.getImage()).into(imgProdukt);
                    preisProdukt.setText(produktAusgewaehlt.getPreis());
                    zusatzstoffeProdukt.setText(produktAusgewaehlt.getZusatzstoffe());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Intent intent = new Intent(Produktseite.this, FehlerDatenbankverbindung.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }
}
