package com.example.foodtogo;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.database.Produkt;
import com.example.foodtogo.viewholders.ProduktViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class UnterkategorieEssen extends AppCompatActivity {

    private DatabaseReference produkt;
    private FirebaseRecyclerAdapter<Produkt, ProduktViewHolder> adapter;
    private FirebaseRecyclerOptions<Produkt> options;

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unterkategorie_essen);

        // Verbindung zu Firebase herstellen
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        produkt = database.getReference("Produkt");

        recyclerView = findViewById(R.id.recycler_view_essensauswahl);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        String essenskategorieID = "";

        if (getIntent() != null)
            essenskategorieID = getIntent().getStringExtra("essenskategorieID");
        if (essenskategorieID != null && !essenskategorieID.isEmpty()){
            final Query query = produkt.orderByChild("essenskategorieID").equalTo(essenskategorieID);

            query.addListenerForSingleValueEvent(new ValueEventListener() {

                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    for (DataSnapshot ds: dataSnapshot.getChildren()){
                        options = new FirebaseRecyclerOptions.Builder<Produkt>().setQuery(query,Produkt.class).build();
                        adapter = new FirebaseRecyclerAdapter<Produkt, ProduktViewHolder>(options) {
                            @Override
                            protected void onBindViewHolder(@NonNull ProduktViewHolder holder, int i, @NonNull Produkt produkt) {

                                holder.produnktname.setText(produkt.getName());
                                Picasso.get().load(produkt.getImage()).into(holder.bildProdukt);
                                final Produkt produktAusgewaehlt = produkt;
                                holder.setClickListener(new ClickListener() {
                                    @Override
                                    public void onClick(View view, int position, boolean isLongClick) {
                                        // Gehe auf die Seite des jeweiligen Produktes
                                        Intent intent = new Intent(UnterkategorieEssen.this, Produktseite.class);
                                        intent.putExtra("essenskategorieID", adapter.getRef(position).getKey());
                                        startActivity(intent);
                                    }
                                });
                            }

                            @NonNull
                            @Override
                            public ProduktViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.essensprodukt_spalte,parent,false);
                                return new ProduktViewHolder(view);
                            }
                        };
                        // Alternative mit Grid-Layout
                        // GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
                        // recyclerView.setLayoutManager(gridLayoutManager);
                        adapter.startListening();
                        recyclerView.setAdapter(adapter);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Intent intent = new Intent(UnterkategorieEssen.this, FehlerDatenbankverbindung.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }
}
