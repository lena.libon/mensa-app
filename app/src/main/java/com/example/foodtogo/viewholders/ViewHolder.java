package com.example.foodtogo.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.ClickListener;
import com.example.foodtogo.R;

public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView ueberbegriffEssen;
    public ImageView bildEssen;

    private ClickListener clickListener;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);

        ueberbegriffEssen = itemView.findViewById(R.id.name_essenskategorie);
        bildEssen = itemView.findViewById(R.id.bild_essenskategorie);

        itemView.setOnClickListener(this);
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        clickListener.onClick(v, getAdapterPosition(), false);
    }

}