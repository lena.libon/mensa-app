package com.example.foodtogo.viewholders;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.ClickListener;
import com.example.foodtogo.R;

public class ProduktViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView produnktname;
    public ImageView bildProdukt;

    private ClickListener clickListener;

    public ProduktViewHolder(@NonNull View itemView) {
        super(itemView);

        produnktname = itemView.findViewById(R.id.name_essensprodukt_spalte);
        bildProdukt = itemView.findViewById(R.id.bild_essensprodukt_spalte);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        clickListener.onClick(v, getAdapterPosition(), false);
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }
}
