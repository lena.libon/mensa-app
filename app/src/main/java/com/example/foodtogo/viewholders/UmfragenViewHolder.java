package com.example.foodtogo.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.ClickListener;
import com.example.foodtogo.R;

public class UmfragenViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView frage, moeglichkeit1, moeglichkeit2;
    public ImageView imgMoeglichkeit1, imgMoeglichkeit2;

    private ClickListener clickListener;

    public UmfragenViewHolder(@NonNull View itemView) {
        super(itemView);

        frage = itemView.findViewById(R.id.frage_umfrage_spalte);
        moeglichkeit1 = itemView.findViewById(R.id.txt_moeglichkeit1_umfrage);
        moeglichkeit2 = itemView.findViewById(R.id.txt_moeglichkeit2_umfrage);

        imgMoeglichkeit1 = itemView.findViewById(R.id.img_moeglichkeit1_umfrage);
        imgMoeglichkeit2 = itemView.findViewById(R.id.img_moeglichkeit2_umfrage);

        itemView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        clickListener.onClick(v, getAdapterPosition(), false);
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }
}