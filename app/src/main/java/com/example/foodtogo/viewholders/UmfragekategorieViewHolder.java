package com.example.foodtogo.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.ClickListener;
import com.example.foodtogo.R;

public class UmfragekategorieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView ueberbegriffUmfrage;
    public ImageView bildUmfrage;

    private ClickListener clickListener;

    public UmfragekategorieViewHolder(@NonNull View itemView) {
        super(itemView);

        ueberbegriffUmfrage = itemView.findViewById(R.id.name_umfragekategorie);
        bildUmfrage = itemView.findViewById(R.id.bild_umfragekategorie);

        itemView.setOnClickListener(this);
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View v) {
        clickListener.onClick(v, getAdapterPosition(), false);
    }

}