package com.example.foodtogo;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.foodtogo.database.Benutzer;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegistrierenCheckPasswort extends AppCompatActivity {

    private Button buttonRegistrieren;
    private EditText passwortRegistrieren;
    private ImageButton passwortAuge;
    private int statusPasswort = 0; // 0: hide, 1: show
    private int versuch = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrieren_check_passwort);

        buttonRegistrieren = findViewById(R.id.btnRegistrieren2);
        passwortAuge = findViewById(R.id.passwort_sichtbar_machen_registrieren2);
        passwortRegistrieren = findViewById(R.id.passwortRegistrieren2);

        // Verbindung zu Firebase herstellen
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference benutzer = database.getReference("Benutzer");

        buttonRegistrieren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = "", passwort = "", handynummer = "", email = "", klassenauswahl = "";

                if (getIntent() != null)
                    name = getIntent().getStringExtra("Name");
                    passwort = getIntent().getStringExtra("Passwort");
                    handynummer = getIntent().getStringExtra("Handynummer");
                    email = getIntent().getStringExtra("Email");
                    klassenauswahl = getIntent().getStringExtra("Klasse");
                if (name != null && !name.isEmpty() && passwort != null && !passwort.isEmpty() && handynummer != null &&
                        klassenauswahl != null && !handynummer.isEmpty() && email != null && !email.isEmpty() && !klassenauswahl.isEmpty()){

                    // Prüfe, ob Passwort richtig angegeben ist
                    if (passwortRegistrieren.getText().toString().isEmpty()){
                        Toast.makeText(RegistrierenCheckPasswort.this, "Bitte gebe dein Passwort erneut an!", Toast.LENGTH_SHORT).show();
                    } else if (!passwortRegistrieren.getText().toString().equals(passwort) && versuch < 3){
                        versuch++;
                        Toast.makeText(RegistrierenCheckPasswort.this, "Passwort falsch - Versuch " + versuch + " von 3", Toast.LENGTH_SHORT).show();
                    } else if (!passwortRegistrieren.getText().toString().equals(passwort) && versuch == 3){
                        Toast.makeText(RegistrierenCheckPasswort.this, "Passwort wurde 3 Mal falsch eingegeben!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(RegistrierenCheckPasswort.this, Registrieren.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Registriere neuen Benutzer
                        Benutzer benutzerNeu = new Benutzer(name, email, passwort, klassenauswahl);
                        benutzerNeu.setHandynummer(handynummer);
                        benutzer.child(handynummer).setValue(benutzerNeu);
                        AktiverNutzer.aktivernutzer = benutzerNeu;

                        Intent intent = new Intent(RegistrierenCheckPasswort.this, Startseite.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                        Toast.makeText(RegistrierenCheckPasswort.this, "Erfolgreich registriert", Toast.LENGTH_SHORT).show();

                        finish();
                    }
                }
            }
        });


    passwortAuge.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (statusPasswort == 0){
                passwortRegistrieren.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                passwortRegistrieren.setSelection(passwortRegistrieren.getText().length());
                statusPasswort = 1;
            } else{
                passwortRegistrieren.setTransformationMethod(PasswordTransformationMethod.getInstance());
                passwortRegistrieren.setSelection(passwortRegistrieren.getText().length());
                statusPasswort = 0;
            }
        }
    });
}
}
