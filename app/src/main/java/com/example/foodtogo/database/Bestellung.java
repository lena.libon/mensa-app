package com.example.foodtogo.database;

import com.example.foodtogo.StatusBestellung;
import com.example.foodtogo.database_bestellung.Teilbestellung;

import java.util.List;

public class Bestellung {
    private String handynummer, name, gesamtsumme, datum, klasse;
    private int pause;
    private List<Teilbestellung> teilbestellungList;
    private StatusBestellung status;

    public Bestellung(String handynummer, String name, int pause, String gesamtsumme, String datum, String klasse, List<Teilbestellung> teilbestellungList){
        this.handynummer = handynummer;
        this.name = name;
        this.pause = pause;
        this.gesamtsumme = gesamtsumme;
        this.datum = datum;
        this.klasse = klasse;
        this.teilbestellungList = teilbestellungList;
        status = StatusBestellung.Bestellt;
    }

    public Bestellung(){

    }

    public String getHandynummer() {
        return handynummer;
    }

    public void setHandynummer(String handynummer) {
        this.handynummer = handynummer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPause() {
        return pause;
    }

    public void setPause(int pause) {
        this.pause = pause;
    }

    public String getGesamtsumme() {
        return gesamtsumme;
    }

    public void setGesamtsumme(String gesamtsumme) {
        this.gesamtsumme = gesamtsumme;
    }

    public List<Teilbestellung> getTeilbestellungList() {
        return teilbestellungList;
    }

    public void setTeilbestellungList(List<Teilbestellung> teilbestellungList) {
        this.teilbestellungList = teilbestellungList;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public StatusBestellung getStatus() {
        return status;
    }

    public void setStatus(StatusBestellung status) {
        this.status = status;
    }

    public String getKlasse() {
        return klasse;
    }

    public void setKlasse(String klasse) {
        this.klasse = klasse;
    }
}
