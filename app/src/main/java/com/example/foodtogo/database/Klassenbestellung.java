package com.example.foodtogo.database;

import com.example.foodtogo.StatusBestellung;
import com.example.foodtogo.database_bestellung.Teilbestellung;

import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class Klassenbestellung {

    private String gesamtsumme, datum, klasse;
    private int pause;
    private List<Teilbestellung> teilbestellungList;
    private StatusBestellung status;

    public Klassenbestellung(String gesamtsumme, String datum, String klasse, int pause, List<Teilbestellung> teilbestellungList){
        this.gesamtsumme = gesamtsumme;
        this.datum = datum;
        this.klasse = klasse;
        this.pause = pause;
        this.teilbestellungList = teilbestellungList;
        status = StatusBestellung.Bestellt;
    }

    public Klassenbestellung(){

    }

    public String getGesamtsumme() {
        return gesamtsumme;
    }

    public void setGesamtsumme(String gesamtsumme) {
        this.gesamtsumme = gesamtsumme;
    }

    public void addToGesamtsumme(String teilsumme){
        String summeBisher = gesamtsumme.replace(',', '.');
        String neueSumme = gesamtsumme.replace(',', '.');
        float summe = Float.parseFloat(summeBisher) + Float.parseFloat(neueSumme);

        Locale locale = Locale.GERMANY;
        NumberFormat numberFormat = NumberFormat.getInstance(locale);

        gesamtsumme = numberFormat.format(summe);
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getKlasse() {
        return klasse;
    }

    public void setKlasse(String klasse) {
        this.klasse = klasse;
    }

    public int getPause() {
        return pause;
    }

    public void setPause(int pause) {
        this.pause = pause;
    }

    public List<Teilbestellung> getTeilbestellungList() {
        return teilbestellungList;
    }

    public void setTeilbestellungList(List<Teilbestellung> teilbestellungList) {
        this.teilbestellungList = teilbestellungList;
    }

    public void addToTeilbestellungList(List<Teilbestellung> teilbestellungList){

        List<Teilbestellung> teilbestellungsListTemp = new LinkedList<>();
        List<String> produktnamen = new LinkedList<>();
        this.teilbestellungList.addAll(teilbestellungList);

        for (Teilbestellung t : this.teilbestellungList){

            // Produkt schon in Warenkorb drinnen -> Nur Anzahl muss erhöht werden
            if (produktnamen.contains(t.getProduktName())){
                for (Teilbestellung t2 : teilbestellungsListTemp){
                    if (t2.getProduktName().equals(t.getProduktName())){
                        t2.setProduktAnzahl(t2.getProduktAnzahl() + t.getProduktAnzahl());
                        break;
                    }
                }
            }

            // Produkt noch nicht in Warenkorb drinnen -> Füge es hinzu
            else{
                produktnamen.add(t.getProduktName());
                teilbestellungsListTemp.add(t);
            }
        }

        this.teilbestellungList = new LinkedList<>();
        teilbestellungList.addAll(teilbestellungsListTemp);
    }

    public StatusBestellung getStatus() {
        return status;
    }

    public void setStatus(StatusBestellung status) {
        this.status = status;
    }
}
