package com.example.foodtogo.database;

public class Essenskategorien {
    private String image, name;

    public Essenskategorien(String image, String name) {
        this.image = image;
        this.name = name;
    }

    public Essenskategorien(){}

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
