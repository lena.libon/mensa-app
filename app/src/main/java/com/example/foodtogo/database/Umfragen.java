package com.example.foodtogo.database;

public class Umfragen {
    private String antwort1, antwort2, frage, img1, img2, umfragekategorieID;

    public Umfragen(){

    }

    public String getAntwort1() {
        return antwort1;
    }

    public void setAntwort1(String antwort1) {
        this.antwort1 = antwort1;
    }

    public String getAntwort2() {
        return antwort2;
    }

    public void setAntwort2(String antwort2) {
        this.antwort2 = antwort2;
    }

    public String getFrage() {
        return frage;
    }

    public void setFrage(String frage) {
        this.frage = frage;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getUmfragekategorieID() {
        return umfragekategorieID;
    }

    public void setUmfragekategorieID(String umfragekategorieID) {
        this.umfragekategorieID = umfragekategorieID;
    }
}
