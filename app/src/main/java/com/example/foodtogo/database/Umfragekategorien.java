package com.example.foodtogo.database;

public class Umfragekategorien {

    private String image, name;

    public Umfragekategorien(String image, String name){
        this.image = image;
        this.name = name;
    }

    public Umfragekategorien(){

    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
