package com.example.foodtogo.database;

public class FAQ {
    private String antwort, frage, kategorie;

    public FAQ(String frage, String antwort, String kategorie){
        this.frage = frage;
        this.antwort = antwort;
        this.kategorie = kategorie;
    }

    public FAQ(){

    }

    public String getAntwort() {
        return antwort;
    }

    public void setAntwort(String antwort) {
        this.antwort = antwort;
    }

    public String getFrage() {
        return frage;
    }

    public void setFrage(String frage) {
        this.frage = frage;
    }

    public String getKategorie() {
        return kategorie;
    }

    public void setKategorie(String kategorie) {
        this.kategorie = kategorie;
    }
}
