package com.example.foodtogo.database;

public class Produkt {
    private String essenskategorieID, image, name, preis, zusatzstoffe;

    public Produkt(String essenskategorieID, String image, String name, String preis, String zusatzstoffe){
        this.essenskategorieID = essenskategorieID;
        this.image = image;
        this.name = name;
        this.preis = preis;
        this.zusatzstoffe = zusatzstoffe;
    }

    public Produkt(){}

    public String getEssenskategorieID() {
        return essenskategorieID;
    }

    public void setEssenskategorieID(String essenskategorieID) {
        this.essenskategorieID = essenskategorieID;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreis() {
        return preis;
    }

    public void setPreis(String preis) {
        this.preis = preis;
    }

    public String getZusatzstoffe() {
        return zusatzstoffe;
    }

    public void setZusatzstoffe(String zusatzstoffe) {
        this.zusatzstoffe = zusatzstoffe;
    }
}
