package com.example.foodtogo.database;

public class Benutzer {

    private String name, email, passwort, handynummer, klasse;

    public Benutzer(String name, String email, String passwort, String klasse){
        this.name = name;
        this.email = email;
        this.passwort = passwort;
        this.klasse = klasse;
    }

    public Benutzer(){}


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getHandynummer() {
        return handynummer;
    }

    public void setHandynummer(String handynummer) {
        this.handynummer = handynummer;
    }

    public String getKlasse() {
        return klasse;
    }

    public void setKlasse(String klasse) {
        this.klasse = klasse;
    }
}
