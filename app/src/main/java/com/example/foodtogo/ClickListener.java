package com.example.foodtogo;

import android.view.View;

public interface ClickListener {
    void onClick(View view, int position, boolean langeGeklickt);
}
