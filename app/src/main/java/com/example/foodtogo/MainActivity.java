package com.example.foodtogo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.foodtogo.database.Benutzer;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.paperdb.Paper;

public class MainActivity extends AppCompatActivity {
    private Button anmeldenBtnStart, registrierenBtnStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // overridePendingTransition(R.anim.fadein, R.anim.fadeout);

        setContentView(R.layout.activity_main);

        anmeldenBtnStart = findViewById(R.id.btnAnmeldenStart);
        registrierenBtnStart = findViewById(R.id.btnRegistrierenStart);

        Paper.init(this);

        anmeldenBtnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        registrierenBtnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registrierSeite = new Intent(MainActivity.this, Registrieren.class);
                startActivity(registrierSeite);
            }
        });

        anmeldenBtnStart.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent anmeldeseite = new Intent(MainActivity.this, Anmelden.class);
                startActivity(anmeldeseite);
            }
        });

        // Überprüfe, ob Benutzer gespeichert ist
        final String handynummer = Paper.book().read("Handynummer");
        final String passwort = Paper.book().read("Passwort");

        if (handynummer != null && passwort != null && !handynummer.isEmpty() && !passwort.isEmpty()){

            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference benutzer = database.getReference("Benutzer");

            // Anmelden
            final ProgressDialog wartenAnzeige = new ProgressDialog(MainActivity.this);
            wartenAnzeige.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            wartenAnzeige.setCancelable(false);
            wartenAnzeige.setMessage("Einen Moment Geduld bitte");
            wartenAnzeige.show();

            benutzer.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    // Auf Nullpointer durchsuchen
                    if (dataSnapshot.child(handynummer).exists()) {
                        wartenAnzeige.dismiss();
                        Benutzer benutzerNeu = dataSnapshot.child(handynummer).getValue(Benutzer.class);
                        benutzerNeu.setHandynummer(handynummer);

                        if (benutzerNeu.getPasswort().equals(passwort)) {
                            Intent intent = new Intent(MainActivity.this, Startseite.class);
                            AktiverNutzer.aktivernutzer = benutzerNeu;
                            startActivity(intent);
                            Toast.makeText(MainActivity.this, "Erfolgreich angemeldet!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        // Passwort falsch
                        else {
                            Toast.makeText(MainActivity.this, "Falsches Passwort", Toast.LENGTH_SHORT).show();
                        }
                    }

                    // Benutzer noch nicht in die Datenbank eingetragen
                    else {
                        wartenAnzeige.dismiss();
                        Intent intent = new Intent(MainActivity.this, Registrieren.class);
                        startActivity(intent);
                        Toast.makeText(MainActivity.this, "Dieser Benutzer existiert nicht!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Intent intent = new Intent(MainActivity.this, FehlerDatenbankverbindung.class);
                    startActivity(intent);
                    finish();
                }
            });

        }
    }
}



