package com.example.foodtogo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Registrieren extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Button registrierenBtn;
    private EditText nameRegistrieren, handynummerRegistrieren, emailRegistrieren, passwortRegistrieren;
    private int statusPasswort = 0; // 0: hide, 1: show
    private ImageButton passwortAuge;
    private Spinner klassenauswahl;
    private String klasse = "5a";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrieren);

        registrierenBtn = findViewById(R.id.btnRegistrieren);
        nameRegistrieren = findViewById(R.id.nameRegistrieren);
        handynummerRegistrieren = findViewById(R.id.handynummerRegistrieren);
        emailRegistrieren = findViewById(R.id.emailRegistrieren);
        passwortRegistrieren = findViewById(R.id.passwortRegistrieren);
        passwortAuge = findViewById(R.id.passwort_sichtbar_machen_registrieren);
        klassenauswahl = findViewById(R.id.klasseRegistrieren);

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this, R.array.klassen, R.layout.spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        klassenauswahl.setAdapter(arrayAdapter);
        klassenauswahl.setOnItemSelectedListener(this);

        // Verbindung zu Firebase herstellen
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference benutzer = database.getReference("Benutzer");

        registrierenBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                final ProgressDialog wartenAnzeige = new ProgressDialog(Registrieren.this);
                wartenAnzeige.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                wartenAnzeige.setCancelable(false);
                wartenAnzeige.setMessage("Einen Moment Geduld bitte");
                wartenAnzeige.show();

                benutzer.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        // Auf Nullpointer durchsuchen
                        if (nameRegistrieren.getText().toString().matches("")){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "Name darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (nameRegistrieren.getText().toString().split(" ").length != 2){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "Name soll im Format Vorname Nachname sein", Toast.LENGTH_SHORT).show();
                        } else if (handynummerRegistrieren.getText().toString().matches("")){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "Handynummer darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (emailRegistrieren.getText().toString().matches("")){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "E-Mail-Adresse darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (emailRegistrieren.getText().toString().split("@").length != 2){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "Bitte gebe eine gültige E-Mail-Adresse an!", Toast.LENGTH_SHORT).show();
                        } else if (passwortRegistrieren.getText().toString().matches("")){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "Passwort darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (passwortRegistrieren.getText().toString().length() < 6){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "Passwort muss mindestens die Länge 6 haben", Toast.LENGTH_SHORT).show();
                        } else if (klasse.toString().equals("xx")){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "Bitte wähle eine Klasse aus!", Toast.LENGTH_SHORT).show();
                        } else if (handynummerRegistrieren.getText().toString().length() < 11){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "Bitte gebe eine gütlige Handynummer an!", Toast.LENGTH_SHORT).show();
                        } else if (handynummerRegistrieren.getText().toString().length() > 13) {
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "Bitte gebe eine gültige Handynummer an!", Toast.LENGTH_SHORT).show();
                        } else if (klasse.equals("xx")){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Registrieren.this, "Bitte wähle deine Klasse aus!", Toast.LENGTH_SHORT).show();
                        } else if (dataSnapshot.child(handynummerRegistrieren.getText().toString()).exists()){
                            // Benutzer schon registriert
                            wartenAnzeige.dismiss();
                            Intent anmeldeSeite = new Intent(Registrieren.this, Anmelden.class);
                            anmeldeSeite.putExtra("Handynummer", handynummerRegistrieren.getText().toString());
                            startActivity(anmeldeSeite);
                        } else {
                            wartenAnzeige.dismiss();
                            Intent intent = new Intent(Registrieren.this, RegistrierenCheckPasswort.class);
                            intent.putExtra("Name", nameRegistrieren.getText().toString());
                            intent.putExtra("Handynummer", handynummerRegistrieren.getText().toString());
                            intent.putExtra("Email", emailRegistrieren.getText().toString());
                            intent.putExtra("Passwort", passwortRegistrieren.getText().toString());
                            intent.putExtra("Klasse", klasse);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        wartenAnzeige.dismiss();
                        Intent fehlerDatenbankverbindung = new Intent(Registrieren.this, FehlerDatenbankverbindung.class);
                        startActivity(fehlerDatenbankverbindung);
                        finish();
                    }
                });
            }
        });

        passwortAuge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (statusPasswort == 0){
                    passwortRegistrieren.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwortRegistrieren.setSelection(passwortRegistrieren.getText().length());
                    statusPasswort = 1;
                } else{
                    passwortRegistrieren.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwortRegistrieren.setSelection(passwortRegistrieren.getText().length());
                    statusPasswort = 0;
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        klasse = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
