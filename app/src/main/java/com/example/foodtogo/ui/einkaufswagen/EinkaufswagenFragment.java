package com.example.foodtogo.ui.einkaufswagen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.AdapterEinkaufswagen;
import com.example.foodtogo.AktiverNutzer;
import com.example.foodtogo.R;
import com.example.foodtogo.database.Bestellung;
import com.example.foodtogo.database_bestellung.DatabaseBestellung;
import com.example.foodtogo.database_bestellung.Teilbestellung;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class EinkaufswagenFragment extends Fragment {

    private RecyclerView recyclerView;
    private TextView preisBestellung;
    private Button btnBestellen, btnWarenkorbLeeren;

    private FirebaseDatabase database;
    private DatabaseReference bestellungen;

    private DatabaseBestellung databaseBestellung;

    private List<Teilbestellung> teilbestellungList = new ArrayList<>();
    private AdapterEinkaufswagen adapterEinkaufswagen;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_einkaufswagen, container, false);


        // Verbinde zu Firebase
        database = FirebaseDatabase.getInstance();
        bestellungen = database.getReference("Bestellung");

        recyclerView = root.findViewById(R.id.produkte_einkaufswagen);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        databaseBestellung = new DatabaseBestellung(getActivity());

        preisBestellung = root.findViewById(R.id.preis_warenkorb);

        btnWarenkorbLeeren = root.findViewById(R.id.btn_warenkorb_leeren);

        btnWarenkorbLeeren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Alert-Nachricht
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("WARNUNG");
                alertDialog.setMessage("Möchtest du den Warenkorb wirklich unwiderruflich löschen?");
                alertDialog.setIcon(R.drawable.ic_delete_black_24dp);

                // Buttons
                alertDialog.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        databaseBestellung.leereWarenkorb();
                        ladeEinkaufswagen();
                        Toast.makeText(getActivity(), "Warenkorb wurde erfolgreich geleert!", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        });

        btnBestellen = root.findViewById(R.id.btn_bestellen_einkaufswagen);

        btnBestellen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (teilbestellungList.isEmpty()){
                    Toast.makeText(getActivity(), "Der Warenkorb ist leer!", Toast.LENGTH_SHORT).show();
                }
                else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Abholzeitraum auswählen");
                    alertDialog.setMessage("Wähle den Tag und die Pause aus, in der du deine Bestellung abholen willst.");
                    alertDialog.setIcon(R.drawable.ic_shopping_cart_black_24dp);

                    final DatePicker datePicker = new DatePicker(getActivity());
                    datePicker.setMinDate(System.currentTimeMillis() - 1000);

                    // Vorbestellung auf 2 Wochen
                    datePicker.setMaxDate(System.currentTimeMillis() + 1209600000);
                    alertDialog.setView(datePicker);

                    alertDialog.setNegativeButton("1. Pause", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Bestellung bestellung = new Bestellung(
                                    AktiverNutzer.aktivernutzer.getHandynummer(),
                                    AktiverNutzer.aktivernutzer.getName(),
                                    1,
                                    preisBestellung.getText().toString(),
                                    datePicker.getDayOfMonth() + "." + (datePicker.getMonth() + 1) + "." + datePicker.getYear(),
                                    AktiverNutzer.aktivernutzer.getKlasse(),
                                    teilbestellungList
                            );

                            // Lade auf Firebase hoch
                            // Primärschlüssel ist HandynummerZeit
                            bestellungen.child(AktiverNutzer.aktivernutzer.getHandynummer() + System.currentTimeMillis()).setValue(bestellung);

                            dialog.cancel();
                            databaseBestellung.leereWarenkorb();
                            ladeEinkaufswagen();
                            Toast.makeText(getActivity(), "Bestellung erfolgreich getätigt!", Toast.LENGTH_SHORT).show();
                        }
                    });

                    alertDialog.setPositiveButton("2. Pause", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Bestellung bestellung = new Bestellung(
                                    AktiverNutzer.aktivernutzer.getHandynummer(),
                                    AktiverNutzer.aktivernutzer.getName(),
                                    2,
                                    preisBestellung.getText().toString(),
                                    datePicker.getDayOfMonth() + "." + (datePicker.getMonth() + 1) + "." + datePicker.getYear(),
                                    AktiverNutzer.aktivernutzer.getKlasse(),
                                    teilbestellungList
                            );

                            // Lade auf Firebase hoch
                            // Primärschlüssel ist HandynummerZeit
                            bestellungen.child(AktiverNutzer.aktivernutzer.getHandynummer() + System.currentTimeMillis()).setValue(bestellung);

                            dialog.cancel();
                            databaseBestellung.leereWarenkorb();
                            ladeEinkaufswagen();
                            Toast.makeText(getActivity(), "Bestellung erfolgreich getätigt!", Toast.LENGTH_SHORT).show();
                        }
                    });

                    alertDialog.setNeutralButton("Abbrechen", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog dialog = alertDialog.create();
                    dialog.show();

                }
            }
        });


        ladeEinkaufswagen();

        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ladeEinkaufswagen();
            }
        });

        refresh(1000);


        return root;

    }

    private void refresh(int milsec) {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                ladeEinkaufswagen();
                refresh(1000);
            }
        };

        handler.postDelayed(runnable, milsec);
    }

    private void ladeEinkaufswagen(){
        // Lade Einkaufswagen
        teilbestellungList = databaseBestellung.getTeilbestellungen();
        adapterEinkaufswagen = new AdapterEinkaufswagen(teilbestellungList, getActivity());
        recyclerView.setAdapter(adapterEinkaufswagen);

        // Berechne Gesamtpreis
        float gesamtpreis = 0;
        for (Teilbestellung teilbestellung:teilbestellungList) {

            // Ändere das Komma in einen Punkt um, um mit den Zahlen als floats zu rechnen
            String teilbestellungPreis = teilbestellung.getPreis().replace(',', '.');
            gesamtpreis += Float.parseFloat(teilbestellungPreis) * Integer.parseInt(teilbestellung.getProduktAnzahl());
        }

        Locale locale = Locale.GERMANY;
        NumberFormat numberFormat = NumberFormat.getInstance(locale);

        preisBestellung.setText(numberFormat.format(gesamtpreis));
    }


    private void loescheProdukt(int order) {
        teilbestellungList.remove(order);
        new DatabaseBestellung(getActivity()).leereWarenkorb();

        for (Teilbestellung teilbestellung : teilbestellungList)
            new DatabaseBestellung(getActivity()).addTeilbestellung(teilbestellung);

        ladeEinkaufswagen();

    }
}
