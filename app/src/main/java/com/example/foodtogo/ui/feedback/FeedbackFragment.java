package com.example.foodtogo.ui.feedback;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.foodtogo.R;

public class FeedbackFragment extends Fragment {
    private Button feedbackSchicken;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_feedback, container, false);

        // Button, um Feedback zu schicken
        feedbackSchicken = root.findViewById(R.id.feedbackSchicken);
        feedbackSchicken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendeEmail();
            }
        });
        return root;
    }

    private void sendeEmail() {
        String[] empfaenger = new String[]{"FoodToGo.app@gmail.com"};
        String betreff = "Feedback";
        String nachricht = "";

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, empfaenger);
        intent.putExtra(Intent.EXTRA_SUBJECT, betreff);
        intent.putExtra(Intent.EXTRA_TEXT, nachricht);

        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent, "Wähle deinen E-Mail-Client aus!"));
    }
}
