package com.example.foodtogo.ui.startseite;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.ClickListener;
import com.example.foodtogo.R;
import com.example.foodtogo.UnterkategorieEssen;
import com.example.foodtogo.database.Essenskategorien;
import com.example.foodtogo.viewholders.ViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class StartseiteFragment extends Fragment {

    private FirebaseDatabase database;
    private DatabaseReference essenskategorien;
    private RecyclerView recyclerView;

    FirebaseRecyclerAdapter<Essenskategorien, ViewHolder> adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_startseite, container, false);


        // Essensauswahl anzeigen, Verbindung zu Firebase herstellen
        recyclerView = root.findViewById(R.id.recycler_view_startseite);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        database = FirebaseDatabase.getInstance();
        essenskategorien = database.getReference("Essenskategorien");

        FirebaseRecyclerOptions<Essenskategorien> options = new FirebaseRecyclerOptions.Builder<Essenskategorien
                >().setQuery(essenskategorien, Essenskategorien.class).build();

        adapter = new FirebaseRecyclerAdapter<Essenskategorien, ViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Essenskategorien model) {

                // Lade Bild und Text
                holder.ueberbegriffEssen.setText(model.getName());
                Picasso.get().load(model.getImage()).into(holder.bildEssen);

                final Essenskategorien ausgewaehltesEssen = model;
                holder.setClickListener(new ClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean langeGeklickt) {
                        // Leite auf die passende Kategorie-Seite um
                        Intent intent = new Intent(getActivity(), UnterkategorieEssen.class);
                        intent.putExtra("essenskategorieID", adapter.getRef(position).getKey());
                        startActivity(intent);
                    }
                });


            }

            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View view = inflater.inflate(R.layout.essenskategorie_spalte, parent, false);
                return new ViewHolder(view);
            }
        };

        adapter.startListening();
        recyclerView.setAdapter(adapter);

        return root;
    }
}

