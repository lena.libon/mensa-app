package com.example.foodtogo.ui.bestellungen;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.foodtogo.R;
import com.example.foodtogo.StatusBestellung;
import com.example.foodtogo.UnterkategorieBestellungen;

public class BestellungenFragment extends Fragment {

    private LinearLayout bestellt, abholbereit, abgeholt;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_bestellungen, container, false);

        bestellt = root.findViewById(R.id.linearlayout_bestellt_bestellungen);

        bestellt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UnterkategorieBestellungen.class);
                intent.putExtra("bestellungsstatus", StatusBestellung.Bestellt);
                startActivity(intent);
            }
        });


        abholbereit = root.findViewById(R.id.linearlayout_abholbereit_bestellungen);

        abholbereit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UnterkategorieBestellungen.class);
                intent.putExtra("bestellungsstatus", StatusBestellung.Abholbereit);
                startActivity(intent);
            }
        });


        abgeholt = root.findViewById(R.id.linearlayout_abgeholt_bestellungen);

        abgeholt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), UnterkategorieBestellungen.class);
                intent.putExtra("bestellungsstatus", StatusBestellung.Abgeholt);
                startActivity(intent);
            }
        });
        return root;
    }
}