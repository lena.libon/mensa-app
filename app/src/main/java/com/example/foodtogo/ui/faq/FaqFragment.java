package com.example.foodtogo.ui.faq;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.foodtogo.ExpandableListAdapter;
import com.example.foodtogo.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FaqFragment extends Fragment {

    private ExpandableListView expandableListView;
    private ExpandableListAdapter expandableListAdapter;

    private FirebaseDatabase database;
    private DatabaseReference faq;

    // Button Kontaktieren
    private Button faqSchicken;

    // TODO Mit Datenbank

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_faq, container, false);

        expandableListView = root.findViewById(R.id.expandable_list_view_faq);

        faqSchicken = root.findViewById(R.id.faq_schicken);

        faqSchicken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendeEmail();
            }
        });

        database = FirebaseDatabase.getInstance();
        faq = database.getReference("FAQ");


        final List<String> ueberbegriffe= new ArrayList<>();
        ueberbegriffe.add("Produkte");
        ueberbegriffe.add("Bestellung");
        ueberbegriffe.add("App");

        HashMap<String, String[][]> fragenUndAntworten;

        String[][] fragenProdukte = {
                {"Kann ich auch Extrawünsche angeben (z.B. Ketchup, Senf)?",
                        "Beim Abholen stehen Ketchup/Senfflaschen, die du benutzen kannst."},
                {"Sind in der App alle Produkte des Pausenverkaufs?",
                        "Ja :)"}
        };

        String[][] fragenBestellung = {
                {"Kann ich mir die Zeit aussuchen, wann ich mein Essen abholen möchte?",
                        "Ja! Klicke auf Bestellen und wähle dann die Pause und den Tag aus."},

                {"Wie kann ich zeigen, dass es meine Bestellung ist?",
                        "Die Bestellung besitzt einen eindeutigen Code. Zeige diesen einfach den Mensabesitzer zusammen mit deinem Benutzerprofil."},

                {"Kann ich meine Bestellung wieder abbestellen (z.B. wenn ich krank bin)?",
                        "Bisher leider noch nicht, wird aber hoffentlich bald kommen."},

                {"Gibt es eine Maximalanzahl an Produkten, die ich bestellen kann?",
                        "Ja. Von jedem Produkt kann man nur höchstens 5 bestellen."}
        };

        String[][] fragenApp = {
                {"Wann kann ich die App offiziell benutzen?",
                        "Dann, wenn wir fertig sind."},

                {"Gibt es die App auf für IOS?",
                        "Nein, leider gibt es die App bisher nur für Android."}
        };

        fragenUndAntworten= new HashMap<>();
        fragenUndAntworten.put(ueberbegriffe.get(0), fragenProdukte);
        fragenUndAntworten.put(ueberbegriffe.get(1), fragenBestellung);
        fragenUndAntworten.put(ueberbegriffe.get(2), fragenApp);

        expandableListAdapter = new ExpandableListAdapter(getActivity(), ueberbegriffe, fragenUndAntworten);
        expandableListView.setAdapter(expandableListAdapter);

        return root;
    }

    private void sendeEmail(){
        String[] empfaenger = new String[]{"FoodToGo.app@gmail.com"};
        String betreff = "FAQ - Neue Frage";
        String nachricht = "";

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, nachricht);
        intent.putExtra(Intent.EXTRA_SUBJECT, betreff);
        intent.putExtra(Intent.EXTRA_EMAIL, empfaenger);

        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent, "Wähle deinen E-Mail-Client aus!"));
    }
}
