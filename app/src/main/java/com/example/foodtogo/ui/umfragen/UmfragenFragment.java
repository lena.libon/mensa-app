package com.example.foodtogo.ui.umfragen;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.ClickListener;
import com.example.foodtogo.R;
import com.example.foodtogo.UnterkategorieUmfrage;
import com.example.foodtogo.database.Umfragekategorien;
import com.example.foodtogo.viewholders.UmfragekategorieViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class UmfragenFragment extends Fragment {

    private FirebaseDatabase database;
    private DatabaseReference umfragekategorien;
    private RecyclerView recyclerView;

    private FirebaseRecyclerAdapter<Umfragekategorien, UmfragekategorieViewHolder> adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_umfragen, container, false);

        recyclerView = root.findViewById(R.id.recycler_view_umfrage);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        database = FirebaseDatabase.getInstance();
        umfragekategorien = database.getReference("Umfragekategorien");

        FirebaseRecyclerOptions<Umfragekategorien> options = new FirebaseRecyclerOptions.Builder<Umfragekategorien>()
                .setQuery(umfragekategorien, Umfragekategorien.class).build();

        adapter = new FirebaseRecyclerAdapter<Umfragekategorien, UmfragekategorieViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull UmfragekategorieViewHolder holder, int position, @NonNull final Umfragekategorien model) {
                holder.ueberbegriffUmfrage.setText(model.getName());
                Picasso.get().load(model.getImage()).into(holder.bildUmfrage);

                final Umfragekategorien ausgewaehlteUmfrage = model;

                holder.setClickListener(new ClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean langeGeklickt) {
                        // Leite auf die Umfrageseite um
                        Intent intent = new Intent(getActivity(), UnterkategorieUmfrage.class);
                        intent.putExtra("umfragekategorieID", adapter.getRef(position).getKey());
                        intent.putExtra("umfragename", model.getName());
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public UmfragekategorieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View view = inflater.inflate(R.layout.umfragekategorie_spalte, parent, false);
                return new UmfragekategorieViewHolder(view);
            }
        };

        adapter.startListening();
        recyclerView.setAdapter(adapter);
        return root;
    }
}