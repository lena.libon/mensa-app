package com.example.foodtogo;

public enum StatusBestellung {
    Bestellt(){
        @Override
        public String toString(){
            return "Bestellt";
        }

        @Override
        public int getBild(){
            return R.drawable.ic_restaurant_white_24dp;
        }
    },

    Abholbereit(){
        @Override
        public String toString(){
            return "Abholbereit";
        }

        @Override
        public int getBild(){
            return R.drawable.ic_access_alarms_black_24dp;
        }
    },

    Abgeholt(){
        @Override
        public String toString(){
            return "Abgeholt";
        }

        @Override
        public int getBild(){
            return R.drawable.ic_check_circle_white_24dp;
        }
    };

    public int getBild(){
        return 0;
    }

}

