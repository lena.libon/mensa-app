package com.example.foodtogo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.database_bestellung.DatabaseBestellung;
import com.example.foodtogo.database_bestellung.Teilbestellung;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class EinkaufswagenViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView produktname, produktpreis;
    private Button produktanzahl;
    private ImageView loeschen;

    private ClickListener clickListener;

    public EinkaufswagenViewHolder(final List<Teilbestellung> teilbestellungList, @NonNull final View itemView, final AdapterEinkaufswagen adapterEinkaufswagen) {
        super(itemView);
        produktname = itemView.findViewById(R.id.produktname_einkaufswagen);
        produktpreis = itemView.findViewById(R.id.produktpreis_einkaufswagen);
        produktanzahl = itemView.findViewById(R.id.produktanzahl_einkaufswagen);
        loeschen = itemView.findViewById(R.id.delete_item_einkaufswagen);

        loeschen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = getAdapterPosition();


                teilbestellungList.remove(position);
                new DatabaseBestellung(itemView.getContext()).leereWarenkorb();

                for (Teilbestellung teilbestellung : teilbestellungList)
                    new DatabaseBestellung(itemView.getContext()).addTeilbestellung(teilbestellung);

                adapterEinkaufswagen.notifyDataSetChanged();

                Toast.makeText(itemView.getContext(), "Produkt wurde aus Warenkorb gelöscht", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public TextView getProduktname() {
        return produktname;
    }

    public void setProduktname(TextView produktname) {
        this.produktname = produktname;
    }

    @Override
    public void onClick(View v) {

    }

    public TextView getProduktpreis() {
        return produktpreis;
    }

    public void setProduktpreis(TextView produktpreis) {
        this.produktpreis = produktpreis;
    }

    public Button getProduktanzahl() {
        return produktanzahl;
    }

    public void setProduktanzahl(Button produktanzahl) {
        this.produktanzahl = produktanzahl;
    }
}
public class AdapterEinkaufswagen extends RecyclerView.Adapter<EinkaufswagenViewHolder>{

    public List<Teilbestellung> teilbestellungList = new ArrayList<>();
    private Context context;

    public AdapterEinkaufswagen(List<Teilbestellung> teilbestellungList, Context context){
        this.teilbestellungList = teilbestellungList;
        this.context = context;
    }

    @NonNull
    @Override
    public EinkaufswagenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EinkaufswagenViewHolder(teilbestellungList, LayoutInflater.from(context).inflate(R.layout.einkaufswagen_spalte, parent, false), this);
    }

    @Override
    public void onBindViewHolder(@NonNull EinkaufswagenViewHolder holder, final int position) {
        // Set Name
        holder.getProduktname().setText(teilbestellungList.get(position).getProduktName());

        // Set Anzahl
        holder.getProduktanzahl().setText(teilbestellungList.get(position).getProduktAnzahl() + "x");

        // Set Preis
        Locale locale = Locale.GERMANY;
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        String preisString = teilbestellungList.get(position).getPreis().replace(',', '.');
        float preis = Float.parseFloat(preisString) * Integer.parseInt(teilbestellungList.get(position).getProduktAnzahl());
        holder.getProduktpreis().setText(numberFormat.format(preis));
    }

    @Override
    public int getItemCount() {
        return teilbestellungList.size();
    }
}
