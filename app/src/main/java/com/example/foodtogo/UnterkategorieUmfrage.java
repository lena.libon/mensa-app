package com.example.foodtogo;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.database.Umfragen;
import com.example.foodtogo.viewholders.UmfragenViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class UnterkategorieUmfrage extends AppCompatActivity {

    private DatabaseReference umfragen;
    private FirebaseRecyclerAdapter<Umfragen, UmfragenViewHolder> adapter;
    private FirebaseRecyclerOptions<Umfragen> options;

    private RecyclerView recyclerView;

    private TextView txtUnterkategorie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unterkategorie_umfrage);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        umfragen = database.getReference("Umfragen");

        txtUnterkategorie = findViewById(R.id.txt_umfragekategorie);

        recyclerView = findViewById(R.id.recycler_view_umfrageauswahl);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        String umfragekategorieID = "";
        String umfragekategorie = "";

        if (getIntent() != null) {
            umfragekategorieID = getIntent().getStringExtra("umfragekategorieID");
            umfragekategorie = getIntent().getStringExtra("umfragename");
        }
        if (umfragekategorie != null && !umfragekategorie.isEmpty())
            txtUnterkategorie.setText(umfragekategorie.toUpperCase());
        if (umfragekategorieID != null && !umfragekategorieID.isEmpty()) {
            final Query query = umfragen.orderByChild("umfragekategorieID").equalTo(umfragekategorieID);

            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        options = new FirebaseRecyclerOptions.Builder<Umfragen>().setQuery(query, Umfragen.class).build();
                        adapter = new FirebaseRecyclerAdapter<Umfragen, UmfragenViewHolder>(options) {

                            @Override
                            protected void onBindViewHolder(@NonNull UmfragenViewHolder holder, int i, @NonNull Umfragen umfragen) {
                                holder.moeglichkeit1.setText(umfragen.getAntwort1());
                                holder.moeglichkeit2.setText(umfragen.getAntwort2());

                                holder.frage.setText(umfragen.getFrage());

                                Picasso.get().load(umfragen.getImg1()).into(holder.imgMoeglichkeit1);
                                Picasso.get().load(umfragen.getImg2()).into(holder.imgMoeglichkeit2);

                                final Umfragen umfrageausgewaehlt = umfragen;

                                holder.setClickListener(new ClickListener() {
                                    @Override
                                    public void onClick(View view, int position, boolean isLongClick) {
                                        Toast.makeText(UnterkategorieUmfrage.this, umfrageausgewaehlt.getAntwort2(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }

                            @NonNull
                            @Override
                            public UmfragenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.umfrage_spalte, parent, false);
                                return new UmfragenViewHolder(view);
                            }
                        };

                        adapter.startListening();
                        recyclerView.setAdapter(adapter);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Intent intent = new Intent(UnterkategorieUmfrage.this, FehlerDatenbankverbindung.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }
}