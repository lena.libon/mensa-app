package com.example.foodtogo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class FehlerDatenbankverbindung extends AppCompatActivity {

    private Button fehlermeldungDatenbankverbindungKontaktBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fehler_datenbankverbindung);

        fehlermeldungDatenbankverbindungKontaktBtn = findViewById(R.id.fehlermeldungDatenbankverbindungKontaktieren);

        fehlermeldungDatenbankverbindungKontaktBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                sendeEmail();
            }
        });
    }

    private void sendeEmail(){
        String[] empfaenger = new String[]{"FoodToGo.app@gmail.com"};
        String betreff = "Fehler bei der Datenbankverbindung";
        String nachricht = "Beschreibung des Fehlers: \n";

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, empfaenger);
        intent.putExtra(Intent.EXTRA_SUBJECT, betreff);
        intent.putExtra(Intent.EXTRA_TEXT, nachricht);

        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent, "Wähle deinen E-Mail-Client aus!"));
    }
}
