package com.example.foodtogo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodtogo.database.Bestellung;

import java.util.List;

public class RecyclerViewAdapterBestellung extends RecyclerView.Adapter<RecyclerViewAdapterBestellung.BestellungViewHolder> {

    private List<Bestellung> bestellungList;
    private List<String> bestellungIDs;
    private LayoutInflater layoutInflater;


    public RecyclerViewAdapterBestellung(Context context, List<Bestellung> bestellungList, List<String> bestellungIDs){
        layoutInflater = LayoutInflater.from(context);
        this.bestellungList = bestellungList;
        this.bestellungIDs = bestellungIDs;
    }


    @NonNull
    @Override
    public BestellungViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.bestellung_spalte, parent, false);
        return new BestellungViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BestellungViewHolder holder, int position) {

        // Bild
        StatusBestellung statusBestellung = bestellungList.get(position).getStatus();
        holder.bildBestellungsart.setImageResource(statusBestellung.getBild());

        // ID
        holder.txtBestellungsID.setText("#" + bestellungIDs.get(position));

        // Gesamtpreis
        holder.txtGesamtpreis.setText(bestellungList.get(position).getGesamtsumme());

        // Name
        holder.bestellungName.setText("Bestellung " + (position + 1));

        // Zeit
        Bestellung bestellungTemp = bestellungList.get(position);
        String temp = bestellungTemp.getDatum() + "\t | \t" + bestellungTemp. getPause() + ". Pause";
        holder.txtZeit.setText(temp);

    }

    @Override
    public int getItemCount() {
        return bestellungList.size();
    }

    public class BestellungViewHolder extends RecyclerView.ViewHolder {

        public ImageView bildBestellungsart;
        public TextView txtBestellungsID, txtGesamtpreis, txtZeit, bestellungName;

        public BestellungViewHolder(@NonNull View itemView) {
            super(itemView);

            bildBestellungsart = itemView.findViewById(R.id.img_bestellungsart);
            txtBestellungsID = itemView.findViewById(R.id.bestell_id_bestellungen);
            txtGesamtpreis = itemView.findViewById(R.id.gesamtpreis_bestellungen);
            txtZeit = itemView.findViewById(R.id.abholzeit_bestellungen);
            bestellungName = itemView.findViewById(R.id.bestellname);
        }
    }


}
