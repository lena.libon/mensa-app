package com.example.foodtogo;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> ueberbegriffeFAQ;
    private HashMap<String, String[][]> fragenMitAntworten;

    public ExpandableListAdapter(Context context, List<String> ueberbegriffeFAQ, HashMap<String, String[][]> fragenMitAntworten) {
        this.context = context;
        this.ueberbegriffeFAQ = ueberbegriffeFAQ;
        this.fragenMitAntworten = fragenMitAntworten;
    }

    @Override
    public int getGroupCount() {
        return ueberbegriffeFAQ.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return fragenMitAntworten.get(ueberbegriffeFAQ.get(groupPosition)).length;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return ueberbegriffeFAQ.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return fragenMitAntworten.get(ueberbegriffeFAQ.get(groupPosition))[childPosition];
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String ueberbegriff = (String) getGroup(groupPosition);
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView =inflater.inflate(R.layout.faq_ueberbegriffe, null);
        }

        TextView ueberbegriffeFaq = convertView.findViewById(R.id.ueberbegriffe_faq);
        ueberbegriffeFaq.setTypeface(null, Typeface.BOLD);
        ueberbegriffeFaq.setText(ueberbegriff);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        String[] frageAntwort = (String[]) getChild(groupPosition, childPosition);
        String frage = frageAntwort[0];
        String antwort = frageAntwort[1];

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.faq_item, null);
        }

        TextView frageFAQ = convertView.findViewById(R.id.frage_faq);
        TextView antwortFAQ = convertView.findViewById(R.id.antwort_faq);

        frageFAQ.setText(frage);
        antwortFAQ.setText(antwort);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
