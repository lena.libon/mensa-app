package com.example.foodtogo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.foodtogo.database.Benutzer;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.paperdb.Paper;

public class Anmelden extends AppCompatActivity {
    private Button anmeldenBtn;
    private EditText handynummerAnmelden, passwortAnmelden;
    private ImageButton passwortAuge;
    private CheckBox angemeldetBleiben;
    private int statusPasswort = 0; // 0: hide, 1: show

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anmelden);

        anmeldenBtn = findViewById(R.id.btnAnmelden);
        handynummerAnmelden = findViewById(R.id.handynummerAnmelden);
        passwortAnmelden = findViewById(R.id.passwortAnmelden);
        passwortAuge = findViewById(R.id.passwort_sichtbar_machen);
        angemeldetBleiben = findViewById(R.id.checkbox_anmelden);

        Paper.init(this);

        String handynummer = "";

        if (getIntent() != null)
            handynummer = getIntent().getStringExtra("Handynummer");

        if (handynummer != null && !handynummer.isEmpty()){
            handynummerAnmelden.setText(handynummer, TextView.BufferType.EDITABLE);
            Toast.makeText(Anmelden.this, "Bitte gebe dein Passwort ein", Toast.LENGTH_SHORT).show();
        }

        // Verbindung zu Firebase herstellen
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference benutzer = database.getReference("Benutzer");

        anmeldenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (angemeldetBleiben.isChecked()){
                    Paper.book().write("Handynummer", handynummerAnmelden.getText().toString());
                    Paper.book().write("Passwort", passwortAnmelden.getText().toString());
                }
                final ProgressDialog wartenAnzeige = new ProgressDialog(Anmelden.this);
                wartenAnzeige.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                wartenAnzeige.setCancelable(false);
                wartenAnzeige.setMessage("Einen Moment Geduld bitte");
                wartenAnzeige.show();

                benutzer.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        // Auf Nullpointer durchsuchen
                        if (handynummerAnmelden.getText().toString().matches("")){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Anmelden.this, "Handynummer darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (passwortAnmelden.getText().toString().matches("")){
                            wartenAnzeige.dismiss();
                            Toast.makeText(Anmelden.this, "Passwort darf nicht leer sein", Toast.LENGTH_SHORT).show();
                        } else if (dataSnapshot.child(handynummerAnmelden.getText().toString()).exists()){
                            // Benutzer schon in Datenbank eingetragen
                            wartenAnzeige.dismiss();
                            Benutzer benutzerNeu = dataSnapshot.child(handynummerAnmelden.getText().toString()).getValue(Benutzer.class);
                            benutzerNeu.setHandynummer(handynummerAnmelden.getText().toString());

                            // Passwort richtig
                            if (benutzerNeu.getPasswort().equals(passwortAnmelden.getText().toString())){
                                AktiverNutzer.aktivernutzer = benutzerNeu;
                                Intent startseite = new Intent(Anmelden.this, Startseite.class);
                                startActivity(startseite);
                                Toast.makeText(Anmelden.this, "Erfolgreich angemeldet!", Toast.LENGTH_SHORT).show();
                                finish();

                                benutzer.removeEventListener(this);
                            }

                            // Passwort falsch
                            else{
                                Toast.makeText(Anmelden.this, "Falsches Passwort!", Toast.LENGTH_SHORT).show();
                            }

                        }

                        // Benutzer noch nicht in die Datenbank eingetragen
                        else{
                            wartenAnzeige.dismiss();
                            Intent registrierenSeite = new Intent(Anmelden.this, Registrieren.class);
                            startActivity(registrierenSeite);
                            Toast.makeText(Anmelden.this, "Dieser Benutzer existiert nicht!", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        wartenAnzeige.dismiss();
                        Intent fehlerDatenbankverbindung = new Intent(Anmelden.this, FehlerDatenbankverbindung.class);
                        startActivity(fehlerDatenbankverbindung);
                        finish();
                    }
                });
            }
        });

        passwortAuge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (statusPasswort == 0){
                    passwortAnmelden.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    passwortAnmelden.setSelection(passwortAnmelden.getText().length());

                    statusPasswort = 1;
                } else{
                    passwortAnmelden.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    passwortAnmelden.setSelection(passwortAnmelden.getText().length());

                    statusPasswort = 0;
                }
            }
        });

    }
}
