package com.example.foodtogo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class SplashScreen extends AppCompatActivity {

    private final int DELAY = 5000;
    private Animation animation_mgm, animation_drache;
    private ImageView logo_mgm, logo_drache, logo_mgm2;
    private MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        logo_mgm = findViewById(R.id.logo_splash_screen_mgm);
        logo_mgm2 = findViewById(R.id.logo2_splash_screen);
        logo_drache = findViewById(R.id.logo_splash_screen_drache);

        animation_drache = AnimationUtils.loadAnimation(this, R.anim.anim_splash_screen_drache);
        animation_mgm = AnimationUtils.loadAnimation(this, R.anim.anim_splash_screen_mgm);

        logo_mgm.setAnimation(animation_mgm);
        logo_mgm2.setAnimation(animation_mgm);
        logo_drache.setAnimation(animation_drache);

        mediaPlayer = MediaPlayer.create(this, R.raw.splashscreen);
        mediaPlayer.start();

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mediaPlayer.release();
                mediaPlayer = null;
            }
        });



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, DELAY);

    }
}
