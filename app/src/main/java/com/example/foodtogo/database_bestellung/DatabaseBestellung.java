package com.example.foodtogo.database_bestellung;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseBestellung extends SQLiteAssetHelper {
    private static String name = "MGMensa.db";
    private static int version = 1;

    public DatabaseBestellung(Context context) {
        super(context, name, null, version);
    }

    public void addTeilbestellung(Teilbestellung teilbestellung){

        String produktIDTemp = teilbestellung.getProduktID();
        String produktNameTemp = teilbestellung.getProduktName();
        String produktAnzahlTemp = teilbestellung.getProduktAnzahl();
        String preisTemp = teilbestellung.getPreis();

        SQLiteDatabase database = getReadableDatabase();

        String abfrage = "INSERT INTO Teilbestellung (produktID,produktName, produktAnzahl, preis) \n VALUES ('" + produktIDTemp + "', '" + produktNameTemp + "', '" + produktAnzahlTemp + "', '" + preisTemp + "');";

        database.execSQL(abfrage);
    }

    public List<Teilbestellung> getTeilbestellungen(){
        String[] spalten = {"produktID", "produktName", "produktAnzahl", "preis"};

        SQLiteDatabase database = getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables("Teilbestellung");
        Cursor cursor = queryBuilder.query(database, spalten, null, null, null, null, null);

        // Gehe nacheinander Teilbestellungen durch und füge sie der Liste ergebnis hinzu
        List<Teilbestellung> ergebnis = new ArrayList<>();
        if (cursor.moveToFirst()){
            do{
                ergebnis.add(new Teilbestellung(
                        cursor.getString(cursor.getColumnIndex("produktID")),
                        cursor.getString(cursor.getColumnIndex("produktName")),
                        cursor.getString(cursor.getColumnIndex("produktAnzahl")),
                        cursor.getString(cursor.getColumnIndex("preis"))));
            } while (cursor.moveToNext());
        }

        return ergebnis;
    }

    public void leereWarenkorb(){
        SQLiteDatabase database = getReadableDatabase();

        // Lösche alle Einträge, aber nicht die database
        String abfrage = String.format("DELETE FROM Teilbestellung");
        database.execSQL(abfrage);
    }
}
