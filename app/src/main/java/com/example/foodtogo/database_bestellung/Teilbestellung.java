package com.example.foodtogo.database_bestellung;

public class Teilbestellung {
    private String produktID, produktName, produktAnzahl, preis;

    public Teilbestellung(String produktID, String produktName, String produktAnzahl, String preis){
        this.produktID = produktID;
        this.produktName = produktName;
        this.produktAnzahl = produktAnzahl;
        this.preis = preis;
    }

    public Teilbestellung(){

    }

    public String getProduktID() {
        return produktID;
    }

    public void setProduktID(String produktID) {
        this.produktID = produktID;
    }

    public String getProduktName() {
        return produktName;
    }

    public void setProduktName(String produktName) {
        this.produktName = produktName;
    }

    public String getProduktAnzahl() {
        return produktAnzahl;
    }

    public void setProduktAnzahl(String produktAnzahl) {
        this.produktAnzahl = produktAnzahl;
    }

    public String getPreis() {
        return preis;
    }

    public void setPreis(String preis) {
        this.preis = preis;
    }
}
