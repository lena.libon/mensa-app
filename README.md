# MGMensa - App zur Essensbestellung beim Pausenverkauf/in der Mensa

![icon](/uploads/aca969a2a7f5b70f4b589b7002a8179d/iconMGMena.png)

## Ziel
Es soll eine App erstellt werden, mit der die Schüler und Lehrer des MGMs das Angebot in der Mensa bzw. dem Pausenverkauf einsehen können und dieses schon im Voraus bestellen können. 

Dadurch ist eine bessere Einschätzung von Seiten der Verkäufer über die benötigte Menge an Essen möglich. Hiermit kann die Essensverschwendung vorgebeugt werden. 
Zudem wird verhindert, dass das Essen schon frühzeitig ausverkauft ist und die Nachfrage nicht vollständig gedeckt werden konnte. 

Die App fördert daher einen maximalen Ausgleich von Angebot und Nachfrage. Vor allem in der Corona-Zeit können durch die App die Schlangen vor dem Pausenverkauf/der Mensa deutlich verkürzt werden.  

## Funktionen 
- **Essensauswahl:** Durch das Pausenverkaufsangebot navigieren, Produkte in Einkaufswagen legen
- **Einkaufswagen:** Produkte in Warenkorb kostenpflichtig bestellen, Abholungsdatum (bis zwei Wochen im Vorraus), sowie Zeit (1. Pause/2. Pause) auswählen
- **Bestellungen:** Bisherige Bestellungen einsehen, die in die drei Kategorien Bestellt, Abholbereit und Abgeholt unterteilt sind
- **Feedback:** Schickt uns Feedback rund um die App
- **Umfragen:** Nehmt an Umfragen teil und entscheidet so, welche nächsten Aktionstage es geben soll, welche neuen Produkte ins Sortiment aufgenommen werden sollen, etc. (funktioniert noch nicht ganz)
- **FAQ:** Beantwortete Fragen rund um den Pausenverkauf und diese App
- Passwort ändern
- Email ändern
- Automatisches Login

## Gallerie
![main](/uploads/d97977d535b19235e0291fb852759448/main.png)
|
![menu](/uploads/60ad92e1f08de7a3212131a9ae9ed7d3/Screenshot_1599501353.png)
![Screenshot_1601842936](/uploads/573ee4bd414b13f29224a8e06cc9c114/Screenshot_1601842936.png)

![Screenshot_1601842956](/uploads/a6fe92ce0ba02295fa05e0cfb2cfce1d/Screenshot_1601842956.png)

![Screenshot_1601842963](/uploads/70c7a7485552544a445c242b7587d3dd/Screenshot_1601842963.png)![Screenshot_1601842763](/uploads/6bec9707c42181ecf72820b4bc04b0a4/Screenshot_1601842763.png)

![Screenshot_1601842829](/uploads/1c4381e390bbc163009eac7526820795/Screenshot_1601842829.png)

![Screenshot_1601842842](/uploads/dc440895a91fdefe1fc0f4c8031645df/Screenshot_1601842842.png)

![Screenshot_1601842871](/uploads/afdbac19af0dbd831c4e2dce9cfdba53/Screenshot_1601842871.png)

![Screenshot_1601842885](/uploads/c654f16f1123cc2ff293021491b9bf08/Screenshot_1601842885.png)

![Screenshot_1601842907](/uploads/2f0876bb79a9ca7b68c9e5325518b1cd/Screenshot_1601842907.png)

![Screenshot_1601842916](/uploads/748b9155f665bc093a7eac511d301a25/Screenshot_1601842916.png)
